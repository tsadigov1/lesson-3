package az.ingress.microservices.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class StudentDTO {

    private String name;
    private String institute;
    private String birthdate;

}
