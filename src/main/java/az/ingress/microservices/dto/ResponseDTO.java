package az.ingress.microservices.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ResponseDTO {

    private Integer responseCode;
    private String responseMessage;
    private Object responseBody;

}
