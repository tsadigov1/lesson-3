package az.ingress.microservices.controller;

import az.ingress.microservices.domain.Student;
import az.ingress.microservices.dto.ResponseDTO;
import az.ingress.microservices.dto.StudentDTO;
import az.ingress.microservices.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/students")
@RequiredArgsConstructor
public class StudentController {

    private final StudentService studentService;

    @PostMapping
    public ResponseEntity<ResponseDTO> create(@RequestBody StudentDTO studentDTO){

        Student student = studentService.create(studentDTO);
        ResponseDTO responseDTO = ResponseDTO.builder()
                .responseCode(201)
                .responseMessage("Student created")
                .responseBody(student)
                .build();

        return ResponseEntity.ok().body(responseDTO);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ResponseDTO> getOne(@PathVariable Long id){

        Optional<Student> student = studentService.getOne(id);
        ResponseDTO responseDTO = ResponseDTO.builder()
                .responseCode(200)
                .responseMessage("Student found")
                .responseBody(student)
                .build();

        return ResponseEntity.ok().body(responseDTO);
    }

    @GetMapping
    public ResponseEntity<ResponseDTO> getAll(){

        List<Student> studentList = studentService.getAll();
        ResponseDTO responseDTO = ResponseDTO.builder()
                .responseCode(200)
                .responseMessage("Student found")
                .responseBody(studentList)
                .build();

        return ResponseEntity.ok().body(responseDTO);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<ResponseDTO> delete(@PathVariable Long id){

        studentService.delete(id);
        ResponseDTO responseDTO = ResponseDTO.builder()
                .responseCode(204)
                .responseMessage("Student deleted")
                .build();

        return ResponseEntity.ok().body(responseDTO);
    }

    @PutMapping
    public ResponseEntity<ResponseDTO> update(@RequestBody Student student){

        Student studentUpdated = studentService.update(student);
        ResponseDTO responseDTO = ResponseDTO.builder()
                .responseCode(201)
                .responseMessage("Student updated")
                .responseBody(studentUpdated)
                .build();

        return ResponseEntity.ok().body(responseDTO);

    }

}
