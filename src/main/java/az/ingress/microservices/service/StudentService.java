package az.ingress.microservices.service;

import az.ingress.microservices.domain.Student;
import az.ingress.microservices.dto.StudentDTO;

import java.util.List;
import java.util.Optional;

public interface StudentService {

    Optional<Student> getOne(Long id);
    List<Student>getAll();
    Student create(StudentDTO studentDTO);
    void delete(Long id);
    Student update(Student student);

}
