package az.ingress.microservices.service;

import az.ingress.microservices.dao.StudentRepo;
import az.ingress.microservices.domain.Student;
import az.ingress.microservices.dto.StudentDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class StudentServiceImpl implements StudentService {

    private DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    private final StudentRepo studentRepo;

    @Override
    public Optional<Student> getOne(Long id) {
        Optional<Student> student = studentRepo.findById(id);
        return student;
    }

    @Override
    public List<Student> getAll() {
        List<Student> studentList = studentRepo.findAll();
        return studentList;
    }

    @Override
    public Student create(StudentDTO studentDTO) {

        LocalDate birthDate = LocalDate.parse(studentDTO.getBirthdate(), formatter);

        Student student = Student.builder()
                .name(studentDTO.getName())
                .institute(studentDTO.getInstitute())
                .birthdate(birthDate)
                .build();
        studentRepo.save(student);
        return student;
    }

    @Override
    public void delete(Long id) {
        studentRepo.deleteById(id);
    }

    @Override
    public Student update(Student student) {
        studentRepo.save(student);
        return student;
    }
}
